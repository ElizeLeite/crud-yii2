<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Filiacao */

$this->title = 'Criar filiação';
$this->params['breadcrumbs'][] = ['label' => 'Filiações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filiacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
