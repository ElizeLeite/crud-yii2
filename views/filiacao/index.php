<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FiliacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Filiações';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filiacao-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="button-add-box">
        <?= Html::a('Adicionar filiação', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'idPessoa',
            'tipo',
            'nome',
            'cpf',
            //'telefone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
