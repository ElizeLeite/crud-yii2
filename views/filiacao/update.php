<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Filiacao */

$this->title = 'Update Filiacao: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Filiações', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="filiacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
