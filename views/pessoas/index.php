<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PessoasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pessoas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pessoas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="button-add-box">
        <?= Html::a('Adicionar Pessoa', ['create'], ['class' => 'button-add btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'nome',
            // 'idade',
            [
              'attribute' => 'idade',
              'value' => function ($data) {
                return $data->idade;
              },
              'label' => 'Idade',
              'filterInputOptions' => ['type' => 'number']

            ],
            'endereco',
            'email:email',

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update} {delete}',
              'buttons' => [
                  'delete' => function($url, $model){
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                          'class' => '',
                          'data' => [
                              'confirm' => 'Você tem certeza que deseja excluir este item?',
                              'method' => 'post',
                          ],
                      ]);
                  }
              ]
            ],
        ],
    ]); ?>


</div>
