<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pessoas-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
       <div class="col-md-4">
         <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
         <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>
       </div>
       <div class="col-md-4">
         <?= $form->field($model, 'idade')->textInput() ?>
         <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
       </div>
     </div>




    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
