<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pessoas".
 *
 * @property int $id
 * @property string|null $nome
 * @property int|null $idade
 * @property string|null $endereco
 * @property string|null $email
 */
class Pessoas extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'pessoas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idade'], 'integer'],
            [['nome'], 'string', 'max' => 20],
            [['sexo'], 'string', 'max' => 10],
            [['endereco'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
            'sexo' => 'Sexo',
            'idade' => 'Idade',
            'endereco' => 'Endereço',
            'email' => 'E-mail',
        ];
    }
}
