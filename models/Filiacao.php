<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filiacao".
 *
 * @property int $id
 * @property int $idPessoa
 * @property int $tipo 1=Pai, 2=Mãe
 * @property string $nome
 * @property string $cpf
 * @property string $telefone
 *
 * @property Pessoas $idPessoa0
 */
class Filiacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filiacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPessoa', 'nome'], 'required'],
            [['idPessoa', 'tipo'], 'integer'],
            [['nome'], 'string', 'max' => 50],
            [['cpf', 'telefone'], 'string', 'max' => 15],
            [['idPessoa'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoas::className(), 'targetAttribute' => ['idPessoa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idPessoa' => 'Código Pessoa',
            'tipo' => 'Tipo',
            'nome' => 'Nome',
            'cpf' => 'CPF',
            'telefone' => 'Telefone',
        ];
    }

    /**
     * Gets query for [[IdPessoa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPessoa0()
    {
        return $this->hasOne(Pessoas::className(), ['id' => 'idPessoa']);
    }
}
